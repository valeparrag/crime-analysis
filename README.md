# Crime Analysis

## **Description** 
Prediction of crime category using variables such as:

1. **Dates:** Processed as year, month, day, day of week, hour
1. **Description:** Processed using NLP TF-IDF
1. **PdDistrict:** District 	
1. **Resolution:** Processed using NLP TF-IDF	
1. **Address:** Processed using NLP TF-IDF
1. **X:** X location	
1. **Y:** Y location


## **Model** 
Using a LightGBM model obtaining a 99.6119% in balanced accuracy, which is a helpful metric for analysing unbalanced problems. A Bayessian Optimization was applied to the hyperparameter tunning of the model.
